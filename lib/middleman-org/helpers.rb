
module Middleman
  module Org

    def self.controller
      @controller ||= nil
    end

    def self.controller=(v)
      @controller = v
    end

    module Helpers
      def self.included(base)
      end

      def basic_helper_example( param )
        "<h1>#{param}</h1>".html_safe
      end

      def org_controller
        ::Middleman::Org.controller
      end

      def org_data
        org_controller.data
      end

      def current_org_article
        article = current_resource
        if article && article.is_a?(OrgArticle)
          article
        else
          nil
        end
      end

      def org_articles
        # binding.pry
        articles = org_data.articles.sort { |a, b|  a.get_meta_data('date') <=> b.get_meta_data('date') }
        articles.reverse
      end

      def articles_count
        org_data.articles.count
      end
    end
  end
end
